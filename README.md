# webcpl

![](https://img.shields.io/badge/written%20in-Python-blue)

A web-based control panel for managing shared PHP virtual hosting.

- Manage sites, routes, SSL certificates, and interfaces
- All configuration stored in embedded sqlite3 database
- Standalone web server based on `bottle`

## Changelog

2014-07-05 r23
- Initial private release
- [⬇️ webcpl_r23.zip](dist-archive/webcpl_r23.zip) *(7.35 KiB)*

